
__all__ = []

try:
    xrange
except NameError:
    xrange = range


from . import gtypes
__all__.extend(gtypes.__all__)
from .gtypes import *

from . import Configure
__all__.extend(Configure.__all__)
from .Configure import *


from . import messenger
__all__.extend(messenger.__all__)
from .messenger import *

from . import storage
__all__.extend(storage.__all__)
from .storage import *

from . import StatusCode
__all__.extend(StatusCode.__all__)
from .StatusCode import *

from . import enumerations
__all__.extend(enumerations.__all__)
from .enumerations import *

from . import parallel
__all__.extend(parallel.__all__)
from .parallel import *

from . import constants
__all__.extend(constants.__all__)
from .constants import *

from . import Rounding
__all__.extend(Rounding.__all__)
from .Rounding import *

from . import RawDictStreamable
__all__.extend(RawDictStreamable.__all__)
from .RawDictStreamable import *


from . import LoopingBounds
__all__.extend(LoopingBounds.__all__)
from .LoopingBounds import *

from . import LimitedTypeList
__all__.extend(LimitedTypeList.__all__)
from .LimitedTypeList import *

from . import npConstants
__all__.extend(npConstants.__all__)
from .npConstants import *

from . import utilities
__all__.extend(utilities.__all__)
from .utilities import *

from . import parsers
__all__.extend(parsers.__all__)
from .parsers import *


__gaugi__version__ = '1.0'

def print_gaugi_version():
  from Gaugi.enumerations import Color
  print( ("%sGaugi core (%s)%s") % (TexColor.CWHITE,__gaugi__version__,TexColor.CEND) )
  print( ("%sMaintainer: jodafons@cern.ch%s") % (TexColor.CWHITE, TexColor.CEND) )
  print( ("%s            wsfreund@cern.ch%s") % (TexColor.CWHITE, TexColor.CEND) )
#print_gaugi_version()
